import React, { Component } from 'react'
import ReactDOM from 'react-dom'


class App extends Component {

  state = {
    response: null,
    loggedIn: sessionStorage.getItem('token'),
  }

  login(e) {
    e.preventDefault()

    this.setState({
      response: 'fetching...'
    })

    fetch('https://us-central1-jwt-token.cloudfunctions.net/login', {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: this.refs.email.value,
        password: this.refs.password.value
      })
    })
      .then(res => {
        return res.json()
      })
      .then(res => {

        if (res.token) {
          sessionStorage.setItem('token', res.token)
        }

        this.setState({
          response: res,
          loggedIn: sessionStorage.getItem('token')
        })

      })
      .catch(err => {
        alert(err)
      })

  }

  logout() {

    this.setState({
      response: null
    })    

    sessionStorage.removeItem('token')

    this.setState({
      loggedIn: sessionStorage.getItem('token')
    })

  }

  getData() {

    this.setState({
      response: 'fetching...'
    })

    fetch('https://us-central1-jwt-token.cloudfunctions.net/getData', {
      headers: {
        'Authorization': `Bearer ${sessionStorage.getItem('token')}`
      }
    })
      .then(res => {
        return res.json()
      })
      .then(res => {
        this.setState({
          response: res
        })
      })
      .catch(err => {
        alert(err)
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs-12">
            <h2 className="text-center">Token based security</h2>
            <hr />
          </div>

          <div className="col-xs-12 col-sm-4 col-sm-offset-4">
            <form onSubmit={this.login.bind(this)}>
              <div className="form-group">
                <label htmlFor="email">Email address</label>
                <input ref="email" type="email" className="form-control" id="email" placeholder="Email" />
              </div>
              <div className="form-group">
                <label htmlFor="password">Password</label>
                <input ref="password" type="password" className="form-control" id="password" placeholder="Password" />
              </div>
              <div className="form-group">
                {!this.state.loggedIn && <button type="submit" className="btn btn-default">Login</button>}
                {this.state.loggedIn && <button type="button" className="btn btn-default" onClick={this.logout.bind(this)}>Logout</button>}
              </div>
            </form>
          </div>

          <div className="col-xs-12 col-sm-4 col-sm-offset-4">
            <button type="button" className="btn btn-default" onClick={this.getData.bind(this)}>Get data</button>
          </div>

          <div className="col-xs-12">
            <hr />
          </div>

          <div className="col-xs-12">
            <h4>Result:</h4>
            <pre style={{ height: 300 }} >{JSON.stringify(this.state.response, null, 2)}</pre>
          </div>
        </div>
      </div>
    )
  }
}





ReactDOM.render(
  <App />,
  document.getElementById('root')
)
