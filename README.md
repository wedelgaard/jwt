# README

### What is this repository for? ###

This repo contains code to demonstrate how JTW token based security works.

### How do I get set up? ###

* clone this repo

###### Start local web server
* Run `npm install -g create-react-app`
* go into project folder and run `npm install` (make sure npm or yarn is installed on your machine)
* run `npm run start`. This will start a local web server at port 3000

###### Deploy backend code
* Run `npm install -g firebase-tools`
* Create an account on [firebase.google.com](https://firebase.google.com)
* Run `firebase login` use your firebase credentials
* Go into the projects `server` folder and run `npm install` (make sure npm/yarn is installed on your machine)
* Deploy the backend code to [firebase](https://firebase.google.com) by running `npm run deploy`

### Who do I talk to? ###

If in doubt of anything contact Michael Wedelgård at michael@wedelgaard.dk