'use strict'

const functions = require('firebase-functions')
const cors = require('cors')
const faker = require('faker')
const jwt = require('jsonwebtoken')

const jwtSecret = 'veryVerySecret'

const fakeDb = {
	user: 'michael@wedelgaard.dk',
	name: 'michael',
	password: 'secret'
}

const validateMail = mailParam => {
	const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
	return regex.test(mailParam)
}

const verifyToken = token => {

	var decoded = false
	
	try {
		decoded = jwt.verify(token.replace('Bearer ', ''), jwtSecret)
	} catch (e) {
		decoded = false
	}
	return decoded

}

//login
exports.login = functions.https.onRequest((req, res) => {

	const corsFn = cors()

	corsFn(req, res, () => {

		if (req.method === 'POST' &&
			validateMail(req.body.email) &&
			fakeDb.user === req.body.email &&
			fakeDb.password === req.body.password) {
			
			const token = jwt.sign({
				user: req.body.email,
				name: fakeDb.name
			}, jwtSecret, { expiresIn: '1h' })

			res.status(200).json({
				user: req.body.email,
				token: token,
				name: fakeDb.name
			})

		}
		else {
			res.status(400).json({error: "Login failed :("})
		}

	})

})

//Get random data for logged in users
exports.getData = functions.https.onRequest((req, res) => {

	const corsFn = cors()

	corsFn(req, res, () => {

		if (req.method === 'GET') {
			
			if (!verifyToken(req.headers.authorization)) {
				res.status(400).json({error: "Not logged in!"})
			}
			else {
				const randomCard = faker.helpers.createCard()

				res.status(200).json(randomCard)
			}

		}
		else {
			res.status(400).json({error: "Bad request :("})
		}

	})

})